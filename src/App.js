import React, {useState} from 'react';
import './App.css';
import Task from "./Components/Task";
import AddTaskForm from "./Components/AddTaskForm";


function App() {

    const [todos, setTodos] = useState([
        {text: "to do homework 53"}, {text: "to watch react-webinar"}
    ]);
    const [text, setText] = useState('');

    const onInputChange = event => {
        setText(event.target.value)
    }
    const submitNewTodo = event => {
        event.preventDefault();
        setText('')
        if (text == '' || text == null) {
            alert("Вы не ввели задачу")
        } else {
            setTodos([...todos, {id: Date.now(), text}]);
        }
    }
    const removeTask = id => {
        const index = todos.findIndex(t => t.id === id);
        const todosCopy = [...todos];
        todosCopy.splice(index, 1);
        setTodos(todosCopy)
    }

    let tasks = todos.map(task => {
        return <Task key={task.id} id={task.id} onButtonClick={removeTask}>{task.text}</Task>
    });

    return (
        <div className="App wrap">
            <h1 className="header">My To Do List</h1>
            <div className="wrap-list">
                <form onSubmit={submitNewTodo}>
                    <AddTaskForm value={text} onTextChange={onInputChange}/>
                </form>
                <ol className="list">
                    {tasks}
                </ol>
            </div>
        </div>
    );
}

export default App;
