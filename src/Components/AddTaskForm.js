import React from 'react';

const AddTaskForm = props => {
    return (
        <input type='text' value={props.value} placeholder="add new task" onChange={props.onTextChange}/>
    );
};

export default AddTaskForm;