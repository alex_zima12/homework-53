import React from 'react';


const Task = props => {
    return (
        <li className="task">
            {props.children}
            <button className="btn" onClick={() => props.onButtonClick(props.id)}>Delete</button>
        </li>
    );
};

export default Task;